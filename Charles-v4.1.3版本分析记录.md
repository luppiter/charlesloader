## **Charles v4.1.3版本分析记录**

--by B.S.

6/27/2017 2:07:52 PM 

##### 最新版v4.1.3相对v.4.0.2比较，明文类名已混淆.

##### 反编译为源码后,找关键点的分析流程:

>1. 在com\xk72\charles\gui\frames\RegisterFrame.java里找到this.bRegister.addActionListener导入的类import com.xk72.charles.gui.frames.NvMh;
2. 在com.xk72.charles.gui.frames.NvMh里找到if (object2 != null) 使他永久为null的函数导入的类import com.xk72.charles.psPJ;
3. 在com.xk72.charles.psPJ里就是关键类。分析关键点HOOK。
4. 用javap -public psPj.class查看给外部类调用的公有函数方法如下:
``` java
public final class com.xk72.charles.psPJ {
  public com.xk72.charles.psPJ();
  public static boolean qIvM();
  public static void mLFE();
  public static java.lang.String tCiz();
  public static java.lang.String qIvM(java.lang.String, java.lang.String);
}
```
> hook掉public static boolean x1()和public static java.lang.String x3()两个方法函数就可以了,当然最好public修饰的都hook掉;

##### 只需要在用javassist时,设置这一句就可以将破解好的class类文件就会dump出来
> CtClass.debugDump = "./charles-bs-cr";
